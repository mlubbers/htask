#!/bin/sh
set -e
if which hlint >/dev/null
then
	hlint acsds htask htask-examples htask-ui-brick htask-ui-wai
else
	curl -sSL https://raw.github.com/ndmitchell/hlint/master/misc/run.sh | sh -s acsds htask htask-examples htask-ui-brick htask-ui-wai
fi
