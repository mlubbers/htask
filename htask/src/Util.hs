module Util where

ifM :: Monad m => m Bool -> m a -> m a -> m a
ifM mp t e = mp >>= \p->if p then t else e
