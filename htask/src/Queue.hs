module Queue (Queue, empty, push, pop, Queue.filter, toList, fromList) where

data Queue a = Queue { 
  inbox :: [a],
  outbox :: [a]
} deriving (Eq, Show)

empty :: Queue a
empty = Queue [] []

push :: a -> Queue a -> Queue a
push e (Queue inb out) = Queue (e:inb) out

pop :: Queue a -> (Maybe a, Queue a)
pop q@(Queue [] []) = (Nothing, q)
pop (Queue inb []) = pop $ Queue [] (reverse inb)
pop (Queue inb (x:xs)) = (Just x, Queue inb xs)

filter :: (a -> Bool) -> Queue a -> Queue a
filter f = fromList . Prelude.filter f . toList

toList :: Queue a -> [a]
toList (Queue inb outb) = inb ++ reverse outb

fromList :: [a] -> Queue a
fromList = foldr push empty
