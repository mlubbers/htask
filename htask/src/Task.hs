module Task where

import GHC.Generics
import Data.Dynamic
import Control.Monad.RWS
import Data.Functor

import Queue as Q

import Task.Value
import ACSDS
import qualified Data.Set as DS
import qualified Data.Map as DM

data Event = ResetEvent | RefreshEvent (DS.Set TaskId) | EditEvent TaskId String | ButtonEvent TaskId Int
  deriving Show

type InstanceId = Int
type TaskId = Int

data TaskState u = TaskState
    { nextTaskId      :: [TaskId]
    , nextInstanceId  :: [InstanceId]
    , tasks           :: DM.Map InstanceId (Task u ())
    , memoryShares    :: DM.Map String Dynamic
    , eventQueue      :: Queue (InstanceId, Event)
    , observers       :: [NRequest (TaskMonad u)]
    , currentInstance :: InstanceId
    , uistates        :: DM.Map InstanceId (DM.Map TaskId u)
    , shutDownFlag    :: Maybe Int
    }

instance Functor (TaskResult u) where
    fmap f = \case
        ValueResult ui tv task -> ValueResult ui (f <$> tv) (f <$> task)
        ExceptionResult msg -> ExceptionResult msg
instance Applicative (TaskResult u) where
    pure a = ValueResult UINone (Stable a) $ Task $ \_->pure $ pure a
    ExceptionResult e <*> _ = ExceptionResult e
    _ <*> ExceptionResult e = ExceptionResult e
    ValueResult _ l _ <*> ValueResult uir r cr
        = ValueResult uir (l <*> r)
        $ let res cont = Task $ \event->runTask cont event <&> \case
                ValueResult ui r' cr' -> ValueResult ui (l <*> r') $ res cr'
                ExceptionResult e -> ExceptionResult e
        in res cr

newtype Task u a = Task { runTask :: Event -> TaskMonad u (TaskResult u a) }
  deriving Functor
newtype TaskMonad u a = TaskMonad { runTaskMonad :: RWST TaskId [String] (TaskState u) IO a }
  deriving
    ( Functor
    , Applicative
    , MonadIO
    , Monad
    , MonadState (TaskState u)
    , MonadReader TaskId
    , MonadWriter [String]
    , MonadFail
    )

runTaskM :: TaskMonad u a -> TaskState u -> IO (a, TaskState u, [String])
runTaskM tm = runRWST (runTaskMonad tm) 0

data TaskResult u a
    = ValueResult (TaskUI u) (TaskValue a) (Task u a)
    | forall e. (Typeable e, Show e) => ExceptionResult e

data TaskCont a b
    = OnValue (TaskValue a -> Maybe b)
    | OnAction String (TaskValue a -> Maybe b)
    | forall e. Typeable e => OnException (e -> b)
    | OnAllExceptions (String -> b)


queueEvent :: Event -> TaskMonad u ()
queueEvent = modify . queueEventTaskState

queueEventTaskState :: Event -> TaskState u -> TaskState u
queueEventTaskState event s = queueEventForInstanceTaskState (currentInstance s) event s

queueEventForInstance :: InstanceId -> Event -> TaskMonad u ()
queueEventForInstance iid event = modify $ queueEventForInstanceTaskState iid event

-- This merges refresh events for the same instance for efficiency
queueEventForInstanceTaskState :: InstanceId -> Event -> TaskState u -> TaskState u
queueEventForInstanceTaskState iid e st = case (pop (eventQueue st), e) of
    ((Just (iid', RefreshEvent s1), q), RefreshEvent s2)
        | iid' == iid -> st { eventQueue = push (iid, RefreshEvent (DS.union s1 s2)) q}
    ((_, q), _) -> st { eventQueue = push (iid, e) q }

getNextTaskId :: TaskMonad u TaskId
getNextTaskId = gets (head . nextTaskId) <* modify (\s->s { nextTaskId = tail $ nextTaskId s })

getNextInstanceId :: TaskMonad u InstanceId
getNextInstanceId = gets (head . nextInstanceId) <* modify (\s->s { nextInstanceId = tail $ nextInstanceId s })

wrapTask :: (Task u a -> Event -> TaskMonad u (TaskResult u a)) -> TaskMonad u (TaskResult u a) -> TaskMonad u (TaskResult u a)
wrapTask wrap task = task <&> \case
    ValueResult ui tv ntask -> ValueResult ui tv $ Task $ wrap ntask
    r -> r

infixl 2 <<@
infixr 2 @>>
(<<@) :: Task u a -> TuneUI u -> Task u a
task <<@ tune = Task $ \event->runTask task event <&> \case
    ValueResult ui tv ntask -> ValueResult (tune ui) tv $ ntask <<@ tune
    r -> r

(@>>) :: TuneUI u -> Task u a -> Task u a
(@>>) = flip (<<@)

data TaskUI u
    = UINone
    | UIHorizontal { horizontalChildUIs :: [TaskUI u]}
    | UIVertical   { verticalChildUIs :: [TaskUI u]}
    | UIView       { viewValue :: String}
    | UIStep       { stepId :: TaskId, stepButtons :: [(Bool, String)], stepChildUI :: TaskUI u, stepUserUI :: u}
    | UIPanel      { panelTitle :: Maybe String, panelChildUI :: TaskUI u}
    | UIEditor     { editorId :: TaskId, editorValue :: String, editorUserUI :: u}
  deriving (Show, Generic)
type TuneUI u = TaskUI u -> TaskUI u

taskUITaskIds :: TaskUI u -> [TaskId]
taskUITaskIds ui = DM.keys $ taskUIMap ui DM.empty

taskUIMap :: TaskUI u -> DM.Map TaskId u -> DM.Map TaskId u
taskUIMap (UIHorizontal uis) = \m->foldr taskUIMap m uis
taskUIMap (UIVertical uis) = \m->foldr taskUIMap m uis
taskUIMap (UIPanel _ u) = taskUIMap u
-- Task UI's are only important when there are actually buttons
taskUIMap (UIStep _ [] ui _) = taskUIMap ui
taskUIMap (UIStep tid _ ui u) = DM.insert tid u . taskUIMap ui
taskUIMap (UIEditor tid _ u) = DM.insert tid u
taskUIMap (UIView _) = id
taskUIMap UINone = id

setUI :: TaskId -> u -> TaskUI u -> TaskUI u
setUI tid u (UIHorizontal uis) = UIHorizontal $ map (setUI tid u) uis
setUI tid u (UIVertical uis) = UIVertical $ map (setUI tid u) uis
setUI tid u (UIPanel mt ui) = UIPanel mt $ setUI tid u ui
setUI tid u (UIStep tid' b ui u')
    | tid == tid' = UIStep tid' b ui u
    | otherwise = UIStep tid' b (setUI tid u ui) u'
setUI tid u (UIEditor tid' v _)
    | tid == tid' = UIEditor tid v u
setUI _ _ a = a

flattenUI :: TaskUI u -> [TaskUI u]
flattenUI ui = flattenUIAcc ui []

flattenUIAcc :: TaskUI u -> [TaskUI u] -> [TaskUI u]
flattenUIAcc a@(UIHorizontal uis) = \acc->foldr flattenUIAcc (a:acc) uis
flattenUIAcc a@(UIVertical uis) = \acc->foldr flattenUIAcc (a:acc) uis
flattenUIAcc a@(UIPanel _ r) = flattenUIAcc r . (a:)
flattenUIAcc a@(UIStep _ _ r _) = flattenUIAcc r . (a:)
flattenUIAcc e = (e:)

panel :: Maybe String -> TuneUI u
panel = UIPanel

none :: TuneUI u
none = const UINone

horizontal :: TuneUI u
horizontal (UIVertical uis) = UIHorizontal uis
horizontal u = u

vertical :: TuneUI u
vertical (UIHorizontal uis) = UIVertical uis
vertical u = u

class Show u => UIProvider u
  where
    provideEditor :: TaskId -> String -> u
    provideStep :: TaskId -> [(Bool, String)] -> TaskUI u -> u

instance UIProvider ()
  where
    provideEditor _ _ = ()
    provideStep _ _ _ = ()
