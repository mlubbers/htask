module Task.Parallel where

import Data.Functor
import Control.Monad.Reader

import Task.Basic
import Task.Value
import Task

infixr 3 -||-
(-||-) :: Task u a -> Task u a -> Task u a
l -||- r = Task $ \event->getNextTaskId >>= \lid->getNextTaskId >>= \rid->par l r lid rid event
  where
    par ltask rtask lid rid event = local (const lid) (runTask ltask event) >>= \case
        ValueResult uil tvl nl -> local (const rid) (runTask rtask event) <&> \case
            ValueResult uir tvr nr -> ValueResult (UIHorizontal [uil, uir]) (taskvalue tvl tvr) $ Task $ par nl nr lid rid
            ExceptionResult msg -> ExceptionResult msg
        ExceptionResult msg -> pure $ ExceptionResult msg

    taskvalue NoValue rv      = rv
    taskvalue lv NoValue      = lv
    taskvalue rv@(Stable _) _ = rv
    taskvalue _ lv@(Stable _) = lv
    taskvalue lv _            = lv

infixl 3 -||
(-||) :: Task u a -> Task u b -> Task u a
l -|| r = l -||- (r @? const NoValue)

infixl 3 ||-
(||-) :: Task u a -> Task u b -> Task u b
l ||- r = (l @? const NoValue) -||- r

anyTask :: [Task u a] -> Task u a
anyTask = foldr (-||-) $ yield () @? const NoValue

eitherTask :: Task u l -> Task u r -> Task u (Either l r)
eitherTask l r = (l @* Left) -||- (r @* Right)

infixr 4 -&&-
(-&&-) :: Task u a -> Task u b -> Task u (a, b)
l -&&- r = Task $ \event->getNextTaskId >>= \lid->getNextTaskId >>= \rid->par l r lid rid event
  where
    par ltask rtask lid rid event = local (const lid) (runTask ltask event) >>= \case
        ValueResult uil tvl nl -> local (const rid) (runTask rtask event) <&> \case
            ValueResult uir tvr nr -> ValueResult (UIHorizontal [uil, uir]) ((,) <$> tvl <*> tvr) $ Task $ par nl nr lid rid
            ExceptionResult msg -> ExceptionResult msg
        ExceptionResult msg -> pure $ ExceptionResult msg

allTasks :: [Task u a] -> Task u [a]
allTasks = foldr (\l a->uncurry (:) <$> (l -&&- a)) $ yield []
