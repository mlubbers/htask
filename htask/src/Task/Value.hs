module Task.Value where

import Control.Applicative
import Control.Monad

data TaskValue a = NoValue | Unstable a | Stable a
  deriving (Functor, Eq, Show)

getVal :: TaskValue a -> a
getVal (Stable a) = a
getVal (Unstable a) = a
getVal NoValue = undefined

getValue :: TaskValue a -> Maybe a
getValue (Stable a) = Just a
getValue (Unstable a) = Just a
getValue NoValue = Nothing

isStable :: TaskValue a -> Bool
isStable (Stable _) = True
isStable _ = False

instance Applicative TaskValue where
    pure = Stable
    _ <*> NoValue = NoValue
    NoValue <*> _ = NoValue
    Stable f <*> Stable a = Stable $ f a
    f <*> a = Unstable $ getVal f $ getVal a

instance Alternative TaskValue where
    empty = NoValue
    NoValue <|> r = r
    l <|> _ = l

instance Monad TaskValue where
    NoValue >>= _ = NoValue
    Stable v >>= a2mb = a2mb v
    Unstable v >>= a2mb = case a2mb v of
        Stable v' -> Unstable v'
        Unstable v' -> Unstable v'
        NoValue -> NoValue

always :: b -> TaskValue a -> Maybe b
always b _ = Just b

never :: b -> TaskValue a -> Maybe b
never _ _ = Nothing

ifValue :: (a -> Bool) -> (a -> b) -> TaskValue a -> Maybe b
ifValue p fun = getValue >=> predicate
  where predicate v = if p v then Just $ fun v else Nothing

ifStable :: (a -> Bool) -> (a -> b) -> TaskValue a -> Maybe b
ifStable p fun (Stable v)
    | p v = Just (fun v)
ifStable _ _ _ = Nothing

withValue :: (a -> Maybe b) -> TaskValue a -> Maybe b
withValue fun = fun <=< getValue

withStable :: (a -> Maybe b) -> TaskValue a -> Maybe b
withStable fun (Stable v) = fun v
withStable _ _ = Nothing
