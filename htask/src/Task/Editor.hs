module Task.Editor where

import ACSDS

import Control.Monad.State as S
import Control.Monad.Reader
import Control.Monad.RWS
import Text.Read
import Task
import Task.SDS
import Task.Value
import Util

import qualified Data.Set as DS
import qualified Data.Map as DM

view :: Show a => a -> Task u a
view a = Task $ pure $ pure $ ValueResult (UIView (show a)) (Unstable a) $ view a

enter :: (UIProvider u, Show a, Read a) => Task u a
enter = realEditor (VS (constShare undefined, nullShare $ const False)) NoValue ""

edit :: (UIProvider u, Show a, Read a) => a -> Task u a
edit a = realEditor (VS (constShare a, nullShare $ const False)) (Unstable a) (show a)

viewSds :: (Readable v, Observable v, Show a) => v (TaskMonad u) () a w -> Task u a
viewSds sds = Task $ readCompletely (UIView "Loading...") NoValue sds () $ \v ev->register sds >> runTask (realView sds v) ev

realView :: (Observable v, Readable v, Show r) => v (TaskMonad u) () r w -> r -> Task u r
realView sds v = Task $ \case
    RefreshEvent tidset -> ifM (asks (`DS.member` tidset))
            -- The refresh is for us, reread the share and update the editor state
            (register sds >> readCompletely (UIView (show v)) (Unstable v) sds () (runTask . realView sds) ResetEvent)
            -- The refresh is not for us
            skip
    -- In all other events we just skip and yield the old values
    _ -> skip
  where skip = pure $ ValueResult (UIView (show v)) (Unstable v) $ realView sds v

editSds :: (UIProvider u, Observable v, Readable v, Writeable v, Show r, Read r) => v (TaskMonad u) () r r -> Task u r
editSds sds = Task $ readCompletely (UIView "Loading...") NoValue sds () $ \v event->register sds >> runTask (realEditor sds (Unstable v) (show v)) event

realEditor :: (UIProvider u, Observable v, Readable v, Writeable v, Show r, Read r) => v (TaskMonad u) () r r -> TaskValue r -> String -> Task u r
realEditor sds a v = Task $ \case
    -- In case of an edit (for us), we write the share and reflect the change in the UI
    EditEvent tid' v' -> do
        tid <- ask
        if tid' == tid && v /= v'
            then lookupOld tid v >>= \u->case readMaybe v' of
                -- The edit didn't produce a valid value, we yield the old value but update the editor state
                Nothing -> do
                    tell ["Read didn't produce a value: ", v', "\n"]
                    pure (ValueResult u a $ realEditor sds a v')
                -- The edit produced a valid value, we write to the SDS and update the editor state
                Just a'
                    -> writeCompletely u (Unstable a') sds () a'
                        (runTask $ realEditor sds (Unstable a') v')
                        ResetEvent
            -- Either the edit event is not for us or the value is the same
            else skip
    -- In case of a refresh (for us), we read the share again
    RefreshEvent tidset -> do
        tid <- ask
        if tid `DS.member` tidset
            then do
                tell ["Task ", show tid, " refreshes\n"]
                -- The refresh is for us, reread the share and update the editor state
                u <- lookupOld tid v
                register sds
                readCompletely u a sds () (\na->pure $ pure $ ValueResult (UIEditor tid (show na) $ provideEditor tid (show na)) (Unstable na) $ realEditor sds (Unstable na) (show na)) ResetEvent
            -- The refresh is not for us
            else skip
    -- In all other events we just skip and yield the old values
    _ -> skip
  where skip = ask >>= \tid->lookupOld tid v >>= \u->pure (ValueResult u a $ realEditor sds a v)

lookupOld :: UIProvider u => TaskId -> String -> TaskMonad u (TaskUI u)
lookupOld tid v = do
    s <- S.get
    pure $ UIEditor tid v $ case DM.lookup tid $ DM.findWithDefault DM.empty (currentInstance s) (uistates s) of
        Nothing -> provideEditor tid v
        Just u -> u
