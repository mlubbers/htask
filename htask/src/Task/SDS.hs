module Task.SDS where

import Control.Monad.State
import Control.Monad.RWS
import Data.Set as DS
import Data.Map as DM
import Data.Dynamic
import Data.Functor

import Task
import Task.Basic
import Task.Value
import ACSDS
import ACSDS.Lens
import ACSDS.Focus

runPViewTTask :: PViewT (TaskMonad u) a -> TaskMonad u a
runPViewTTask m = do
    obs <- gets observers
    (a, nobs) <- runStateT (runPViewT m) obs
    modify (\s->s { observers=nobs })
    pure a

withShared :: (Show a, Typeable a) => a -> (forall v. (Readable v, Writeable v, Observable v) => v (TaskMonad u) () a a -> Task u b) -> Task u b
withShared initial tfun = Task $ \event->do
    tid <- ask
    ntid <- getNextTaskId
    ws ntid (tfun $ lShare tid) event
  where
    lShare tid = removeMaybe initial $ sdsFocus tid localShare
    ws ntid task event = wrapTask (ws ntid) $ local (const ntid) (runTask task event)

writeCompletely :: Writeable v => TaskUI u -> TaskValue a -> v (TaskMonad u) p r w -> p -> w -> (Event -> TaskMonad u (TaskResult u a)) -> Event -> TaskMonad u (TaskResult u a)
writeCompletely oui otv osds op ow ocont oevent = wc oui otv osds op ow ocont ResetEvent
  where
    wc :: Writeable v => TaskUI u -> TaskValue a -> v (TaskMonad u) p r w -> p -> w -> (Event -> TaskMonad u (TaskResult u a)) -> Event -> TaskMonad u (TaskResult u a)
    wc ui tv sds p w cont ResetEvent = runPViewTTask (write sds p w) >>= \case
        Written _ -> cont oevent
        Writing nsds -> pure $ ValueResult ui tv $ Task $ wc ui tv nsds p w cont
    wc ui tv sds p w cont (RefreshEvent set) = do
        tid <- ask
        if DS.member tid set then wc ui tv sds p w cont ResetEvent else pure $ ValueResult ui tv $ Task $ wc ui tv sds p w cont
    wc ui tv sds p w cont _ = pure $ ValueResult ui tv $ Task $ wc ui tv sds p w cont

readCompletely :: Readable v => TaskUI u -> TaskValue a -> v (TaskMonad u) p r w -> p -> (r -> Event -> TaskMonad u (TaskResult u a)) -> Event -> TaskMonad u (TaskResult u a)
readCompletely oui otv osds op ocont oevent = rc oui otv osds op ocont ResetEvent
  where
    rc :: Readable v => TaskUI u -> TaskValue a -> v (TaskMonad u) p r w -> p -> (r -> Event -> TaskMonad u (TaskResult u a)) -> Event -> TaskMonad u (TaskResult u a)
    rc ui tv sds p cont ResetEvent = readR sds p >>= \case
        Read r -> cont r oevent
        Reading nsds -> pure $ ValueResult ui tv $ Task $ rc ui tv nsds p cont
    rc ui tv sds p cont (RefreshEvent set) = do
        tid <- ask
        if DS.member tid set then rc ui tv sds p cont ResetEvent else pure $ ValueResult ui tv $ Task $ rc ui tv sds p cont
    rc ui tv sds p cont _ = pure $ ValueResult ui tv $ Task $ rc ui tv sds p cont

sdsGet :: Readable v => v (TaskMonad u) () r w -> Task u r
sdsGet sds = Task $ readCompletely UINone NoValue sds () $ runTask . yield

sdsSet :: Writeable v => v (TaskMonad u) () r w -> w -> Task u w
sdsSet sds w = Task $ writeCompletely UINone NoValue sds () w $ runTask (yield w)

memoryShareInternal :: (Show a, Typeable a) => VSource (TaskMonad u) String (Maybe a) (Maybe a)
memoryShareInternal = VS (VRS rfun, VWS "memoryShareInternal" wfun)
  where
    rfun :: Typeable a => String -> TaskMonad u (Maybe a)
    rfun p = do
        tell ["memoryshare: " ++ show p ++ " is read"]
        gets $ \s->DM.lookup p (memoryShares s) >>= fromDynamic

    wfun :: (Typeable a, Show a) => String -> Maybe a -> TaskMonad u (String -> Bool)
    wfun p Nothing = do
        tell ["memoryshare: " ++ show p ++ " is emptied"]
        modify (\s->s { memoryShares = DM.delete p $ memoryShares s })
        pure (p==)
    wfun p (Just w) = do
        tell ["memoryshare: " ++ show p ++ " is written: " ++ show w]
        modify (\s->s { memoryShares = DM.insert p (toDyn w) $ memoryShares s })
        pure (p==)

memoryShare :: (Show a, Typeable a) => VBifocus VSource (TaskMonad u) String (Maybe a) (Maybe a)
memoryShare = sdsTranslate "mem" ("mem:"++) memoryShareInternal

localShare :: (Show a, Typeable a) => VBifocus VSource (TaskMonad u) TaskId (Maybe a) (Maybe a)
localShare = sdsTranslate "local" (("local:"++) . show) memoryShareInternal

watch :: (Readable v, Observable v) => v (TaskMonad u) () r w -> Task u r
watch sds = Task $ watch' NoValue sds sds
  where
    watch' :: (Readable rv, Readable ov, Observable ov) => TaskValue r -> rv (TaskMonad u) () r w -> ov (TaskMonad u) () r w -> Event -> TaskMonad u (TaskResult u r)
    watch' oval rsds osds ResetEvent = realWatch oval rsds osds
    watch' oval rsds osds (RefreshEvent set) = do
        tid <- ask
        if DS.member tid set then realWatch oval rsds osds else pure $ ValueResult UINone oval $ Task $ watch' oval rsds osds
    watch' oval rsds osds _ = pure $ ValueResult UINone oval $ Task $ watch' oval rsds osds

    realWatch :: (Readable rv, Readable ov, Observable ov) => TaskValue r -> rv (TaskMonad u) () r w -> ov (TaskMonad u) () r w -> TaskMonad u (TaskResult u r)
    realWatch oval rsds osds
        =  register osds
        >> readR rsds () <&> \case
            Read r -> let nval = Unstable r in ValueResult UINone nval $ Task $ watch' nval osds osds
            Reading nrsds -> ValueResult UINone oval $ Task $ watch' oval nrsds osds

register :: Observable v => v (TaskMonad u) () r w -> TaskMonad u ()
register osds = do
    tid <- ask
    tell ["task " ++ show tid ++ "registers to " ++ sdsname osds]
    runPViewTTask (observe osds () ("req" ++ show tid) $ do queueEvent $ RefreshEvent $ DS.singleton tid)
