module Task.Eval where

import Control.Monad.RWS

import Data.Time
import Data.Maybe
import Data.List(sort)
import Queue as Q

import ACSDS
import ACSDS.Source
import Task
import Task.Basic
import Task.SDS
import qualified Data.Map as DM

initialTaskState :: TaskState u
initialTaskState = TaskState
    { currentInstance = 0
    , nextTaskId      = [1..]
    , nextInstanceId  = [0..]
    , tasks           = DM.empty
    , memoryShares    = DM.empty
    , observers       = []
    , uistates        = DM.empty
    , eventQueue      = Q.empty
    , shutDownFlag    = Nothing
    }

initInstance :: Task u a -> TaskMonad u InstanceId
initInstance task = do
    (iid:iids) <- gets nextInstanceId
    modify $ \s->
        s { tasks          = DM.insert iid (task @! ()) $ tasks s
          , nextInstanceId = iids
          , uistates       = DM.insert iid DM.empty $ uistates s
          , eventQueue     = Q.push (iid, ResetEvent) $ eventQueue s
          }
    pure iid

deinitInstance :: InstanceId -> TaskMonad u ()
deinitInstance iid = modify $ \s->s
    { tasks      = DM.delete iid $ tasks s
    , eventQueue = Q.filter ((/=iid) . fst) $ eventQueue s
    , uistates   = DM.delete iid $ uistates s
    }

taskLoop :: DM.Map InstanceId (TaskUI u) -> TaskState u -> IO (DM.Map InstanceId (TaskUI u), TaskState u, [String])
taskLoop uis = runTaskM $ do
    -- Update the current time
    utctime <- liftIO getCurrentTime
    Written _ <- runPViewTTask (write currentDateTimeShare () utctime)
    tell [">" ++ show utctime ++ ": taskLoop start\n"]
    st <- get
    tell ["memshares: " ++ show (memoryShares st)]
    tell ["observers: " ++ show (sort $ map show $ observers st)]
    -- Process all the events
    uim <- loop uis
    -- Update the task state with all the UIs
    modify (\s->s { uistates = DM.foldrWithKey updateUIs (uistates s) uim})
    pure uim
  where
    updateUIs :: InstanceId -> TaskUI u -> DM.Map InstanceId (DM.Map TaskId u) -> DM.Map InstanceId (DM.Map TaskId u)
    updateUIs iid ui = DM.alter (Just . taskUIMap ui . fromMaybe DM.empty) iid

    loop :: DM.Map InstanceId (TaskUI u) -> TaskMonad u (DM.Map InstanceId (TaskUI u))
    loop m = gets (pop . eventQueue) >>= \case                    -- Check for events
        (Nothing, _) -> do
            tell ["no events, done"]
            pure m
        (Just (iid, e), q') -> do
            modify (\s->s { eventQueue = q' })                    -- Pop event from the queue
            tell ["process event for instance " ++ show iid ++ ": " ++ show e]
            gets (DM.lookup iid . tasks) >>= \case             -- Get the instance for the task
                Nothing   -> fail "Event for instance that is gone?"
                Just task -> do
                    modify (\s->s { currentInstance = iid })      -- Set the current instance
                    tvr <- runTask task e                         -- Run the actual task
                    ui <- case tvr of                             -- Check the result and either update the UI or throw an error
                        ValueResult ui _ ntask -> ui <$ modify (\s->s { tasks = DM.insert iid ntask $ tasks s})
                        ExceptionResult msg    -> fail $ "Exception in task instance " ++ show iid ++ ": " ++ show msg
                    loop (DM.insert iid ui m)                     -- Recurse to check for more events

nudgeAsyncShares :: TaskMonad u ()
nudgeAsyncShares = do
    obss <- gets observers
    sequence_ [m | NRequest _ _ m _<-obss]
