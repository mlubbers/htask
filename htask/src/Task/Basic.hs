module Task.Basic where

import Data.Dynamic
import Data.Functor
import Control.Monad.State

import Task
import Task.Value

yield :: a -> Task u a
yield a = Task $ pure $ pure $ ValueResult UINone (Stable a) $ yield a

throw :: (Typeable e, Show e) => e -> Task u a
throw e = Task $ pure $ pure $ ExceptionResult e

taskIO :: IO a -> Task u a
taskIO io = Task $ \event->liftIO io >>= \a->runTask (yield a) event

shutDown :: Int -> Task u ()
shutDown c = Task $ \event->modify (\s->s { shutDownFlag=Just c }) >> runTask (yield ()) event

stopWhenStable :: Task u a -> Task u a
stopWhenStable task = Task $ runTask task >=> \case
    ValueResult ui tv ntask -> do
       when (isStable tv) $ modify $ \s->s { shutDownFlag=Just 0 }
       pure $ ValueResult ui tv $ stopWhenStable ntask
    ExceptionResult msg -> pure $ ExceptionResult msg

infixl 1 @?, @*, @!
(@?) :: Task u a -> (TaskValue a -> TaskValue b) -> Task u b
task @? f = Task $ \event->runTask task event <&> \case
    ValueResult ui tv ntask -> ValueResult ui (f tv) (ntask @? f)
    ExceptionResult msg -> ExceptionResult msg

(@*) :: Task u a -> (a -> b) -> Task u b
task @* f = task @? fmap f

(@!) :: Task u a -> b -> Task u b
task @! v = task @* const v
