module Task.Sequential where

import Data.Dynamic
import qualified Data.Map as DM
import Data.Foldable
import Data.Maybe
import Control.Monad.Reader
import Control.Monad.State

import Task
import Task.Value
import Task.Basic

infixl 1 >>*, >>-, >-|, >>~, >~|, >?|, >>?
(>>*) :: UIProvider u => Task u a -> [TaskCont a (Task u b)] -> Task u b
ltask >>* cs = Task $ \event->do
    ltid <- getNextTaskId
    btid <- getNextTaskId
    step ltid btid ltask event
  where
    buttons tval = [(isJust $ f tval, lbl) | OnAction lbl f<-cs]

    lookupOld :: UIProvider u => TaskId -> [(Bool, String)] -> TaskUI u -> TaskMonad u (TaskUI u)
    lookupOld tid bttns ui = do
        s <- get
        pure $ UIStep tid bttns ui $ case DM.lookup tid $ DM.findWithDefault DM.empty (currentInstance s) (uistates s) of
            Nothing -> provideStep tid bttns ui
            Just u -> u

    step taskIdLeft taskIdButtons task event@(ButtonEvent tid' idx)
        -- Button event for us: process, otherwise pass to child
        | tid' == taskIdButtons = local (const taskIdLeft) (runTask task event) >>= \case
            ValueResult ui tv ntask -> case matchButton idx tv cs of
                Nothing   -> lookupOld taskIdButtons (buttons tv) ui >>= \u->pure (ValueResult u NoValue $ Task $ step taskIdLeft taskIdButtons ntask)
                Just cont -> queueEvent ResetEvent >> pure (ValueResult UINone NoValue cont)
            ExceptionResult exc -> case asum $ map (matchException exc) cs of
                Nothing   -> pure $ ExceptionResult exc
                Just cont -> queueEvent ResetEvent >> pure (ValueResult UINone NoValue cont)
    step taskIdLeft taskIdButtons task event = local (const taskIdLeft) (runTask task event) >>= \case
        ValueResult ui tv ntask -> case asum $ map (matchValue tv) cs of
            Nothing   -> lookupOld taskIdButtons (buttons tv) ui >>= \u->pure (ValueResult u NoValue $ Task $ step taskIdLeft taskIdButtons ntask)
            Just cont -> queueEvent ResetEvent >> pure (ValueResult UINone NoValue cont)
        ExceptionResult exc -> case asum $ map (matchException exc) cs of
            Nothing   -> pure $ ExceptionResult exc
            Just cont -> queueEvent ResetEvent >> pure (ValueResult UINone NoValue cont)

    matchButton :: Int -> TaskValue a -> [TaskCont a b] -> Maybe b
    matchButton 0 tv (OnAction _ f:_) = f tv
    matchButton n tv (OnAction _ _:rest) = matchButton (n-1) tv rest
    matchButton n tv (_:rest) = matchButton n tv rest
    matchButton _ _ [] = Nothing

    matchValue :: TaskValue a -> TaskCont a b -> Maybe b
    matchValue tv (OnValue f) = f tv
    matchValue _ _ = Nothing

    matchException :: (Typeable e, Show e) => e -> TaskCont a b -> Maybe b
    matchException exc (OnAllExceptions f) = Just $ f $ show exc
    matchException exc (OnException f) = case fromDynamic (toDyn exc) of
        Nothing -> Nothing
        Just v  -> Just $ f v
    matchException _ _ = Nothing

(>>-) :: UIProvider u => Task u a -> (a -> Task u b) -> Task u b
l >>- r = l >>* [OnValue $ withStable $ Just . r]

(>-|) :: UIProvider u => Task u a -> Task u b -> Task u b
l >-| r = l >>- const r

(>>~) :: UIProvider u => Task u a -> (a -> Task u b) -> Task u b
l >>~ r = l >>* [OnValue $ withValue $ Just . r]

(>~|) :: UIProvider u => Task u a -> Task u b -> Task u b
l >~| r = l >>~ const r

(>>?) :: UIProvider u => Task u a -> (a -> Task u b) -> Task u b
l >>? r = l >>*
    [ OnAction "Continue" $ withValue $ Just . r
    , OnValue $ withStable $ Just . r
    ]

(>?|) :: UIProvider u => Task u a -> Task u b -> Task u b
l >?| r = l >>? const r

foreverTask :: UIProvider u => Task u a -> Task u a
foreverTask t = t >-| foreverTask t

foreverSt :: UIProvider u => a -> (a -> Task u a) -> Task u a
foreverSt st = foreverStIf st (const True)

foreverIf :: UIProvider u => (a -> Bool) -> Task u a -> Task u a
foreverIf p task = task >>- \v->foreverStIf v p (const task)

foreverStIf :: UIProvider u => a -> (a -> Bool) -> (a -> Task u a) -> Task u a
foreverStIf st p task = task st >>- \nst->if p nst then foreverStIf nst p task else yield nst

try :: (Typeable e, UIProvider u) => Task u a -> (e -> Task u a) -> Task u a
try task except = task >>* [OnException except]

catchAll :: UIProvider u => Task u a -> (String -> Task u a) -> Task u a
catchAll task except = task >>* [OnAllExceptions except]
