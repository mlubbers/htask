module Task.Eval.Repl (doTaskReplExit, doTaskRepl, ReplUI) where

import Task
import Task.Eval

import System.Exit
import System.IO
import System.IO.Error
import qualified Data.Map as DM
import Data.Maybe as DM
import Text.Read

data Action = Edit TaskId String | Button TaskId Int | Step | Stop
  deriving (Show, Read, Eq)

type ReplUI = ()

doTaskReplExit :: Task ReplUI a -> IO ()
doTaskReplExit t = doTaskRepl t >>= \case
    0 -> exitSuccess
    n -> exitWith $ ExitFailure n

doTaskRepl :: Task ReplUI a -> IO Int
doTaskRepl task = flip catchIOError (\e->if isEOFError e then 0 <$ hPutStr stderr "\nReceived EOF\n" else ioError e) $ do
    hSetBuffering stdout NoBuffering
    (_, st, lg) <- runTaskM (initInstance task) initialTaskState
    mapM_ (hPutStr stderr) lg
    repl DM.empty st
  where
    repl :: DM.Map InstanceId (TaskUI ReplUI) -> TaskState ReplUI -> IO Int
    repl uis st = do
        (uis', st', lg) <- taskLoop uis st
        mapM_ (hPutStr stderr) lg
        case shutDownFlag st' of
            Just n -> putStrLn "Task issued shutdown" >> pure n
            Nothing
                -> mapM_ putStr (mkUI 0 (getInstanceZero uis') [])
                >> readCommand >>= \case
                    Edit tid v -> repl uis' $ queueEventTaskState (EditEvent tid v) st'
                    Button tid n -> repl uis' $ queueEventTaskState (ButtonEvent tid n) st'
                    Step -> repl uis' st'
                    Stop -> putStrLn "Stopped" >> pure 0

    getInstanceZero :: DM.Map InstanceId (TaskUI ReplUI) -> TaskUI ReplUI
    getInstanceZero m = fromMaybe (UIView $ "No instance 0 in: " ++ show m) $ DM.lookup 0 m

readCommand :: IO Action
readCommand = do
    putStr "> "
    line <- getLine
    case readMaybe line of
        Nothing -> do
            putStrLn "Unknown command, available:"
            putStrLn "\tEdit taskId \"value\" -- Update the value in an editor"
            putStrLn "\tButton taskId n     -- Press the nth button for a task"
            putStrLn "\tStep                -- Do one more step"
            putStrLn "\tStop                -- Stop execution"
            readCommand
        Just c -> pure c

indent :: Int -> [String] -> [String]
indent i acc = replicate i '\t':acc

isperse :: a -> [a] -> [a] -> [a]
isperse _ [] acc = acc
isperse _ [x] acc = x:acc
isperse sep (x:xs) acc = x:sep:isperse sep xs acc

mkUI :: Int -> TaskUI () -> [String] -> [String]
mkUI _ UINone acc = acc
mkUI i (UIView s) acc = indent i $ "view: ":s:"\n":acc
mkUI i (UIStep _ [] u _) acc = mkUI i u acc
mkUI i (UIStep tid buttons u _) acc
    = indent i $ "step (":show tid:")\n"
    : mkUI (i+1) u
    ( indent i ( "[":isperse ", " (zipWith renderButton buttons [0 :: Int ..])
    ( "]\n":acc)))
  where renderButton (enabled, button) idx = button ++ "(" ++ (if enabled then show idx else "disabled") ++ ")"
mkUI i (UIEditor tid v _) acc = indent i $ "editor (":show tid:"): ":v:"\n":acc
mkUI i (UIHorizontal u) acc = foldr (mkUI i) acc u
mkUI i (UIVertical u) acc = foldr (mkUI i) acc u
mkUI i (UIPanel Nothing u) acc = mkUI i u acc
mkUI i (UIPanel (Just title) u) acc = indent i $ "|":title:"|\n":mkUI i u acc
