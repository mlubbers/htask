# hTask

hTask is a repository containing source code for asynchronous class-based shared data sources.
Furthermore, it shows a practical application of them in the form of hTask, a TOP framework embedded in Haskell.

Packages:

- [acsds](acsds): Asynchronous class-based shared data sources
- [htask](htask): The hTask framework, a TOP framework embedded in Haskell
- [htask-examples](htask-examples): A collection of example programs for hTask
- [htask-ui-brick](htask-ui-brick): A single-user TUI for hTask using the [brick](https://github.com/jtdaugherty/brick/) library.
- [htask-ui-wai](htask-ui-wai): A multi-user web UI for hTask using the [wai and warp](https://github.com/yesodweb/wai) libraries

## Asynchronous class-based shared data sources [acsds](acsds)

### Shared data sources

#### Uniform data sources (UDS)

#### Shared data sources (SDS)

#### Class-based shared data sources (CSDS)

#### Asynchronous class-based shared data sources (ACSDS)

### Publications

- H. Böhm, ‘Asynchronous Actions in a Synchronous World’, Master’s Thesis, Radboud University, Nijmegen, 2019.
- L. Domoszlai, B. Lijnse, and R. Plasmeijer, ‘Parametric Lenses: Change notification for Bidirectional Lenses’, New York, NY, USA, 2014. doi: 10.1145/2746325.2746333.
- S. Michels and R. Plasmeijer, ‘Uniform data sources in a functional language’, Nijmegen, 2012. Draft presented at Symposium on Trends in Functional Programming, TFP ’12.
- S. Michels, ‘Building iTask Applications’, Master’s Thesis, Radboud University, Nijmegen, 2010.

## hTask

The hTask framework is a TOP framework embedded in Haskell for educational and academic purposes.
It features a minimal set of shallowly embedded combinators and both the UI and the SDS system are completely orthogonal to the framework itself.

The Shared Data Sources are provided by the [acsds](../acsds) library.

This package provides a very simple single user REPL UI but [htask-ui-brick](../htask-ui-brick) and [htask-ui-wai](../htask-ui-wai) provide a single user TUI and a multi-user web interface respectively.

## Licence

## Authors

Mart Lubbers (mart@cs.ru.nl)
