module ACSDS where

import Data.Dynamic
import Control.Monad.State
import Control.Monad.Writer
import Data.Kind

type Invalidate p = p -> Bool

newtype PViewT m a = PViewT { runPViewT :: StateT [NRequest m] m a }
  deriving (Functor, Applicative, Monad, MonadState [NRequest m])
instance MonadTrans PViewT where lift = PViewT . lift

type SDSId = String
data NRequest m = NRequest SDSId String (m ()) Dynamic
instance Show (NRequest m) where
    show (NRequest sid str _ dyn) = "NRequest for sds " ++ sid ++ " from " ++ str ++ " (" ++ show dyn ++ ")"
data NEvent where
    NEvent :: Typeable p => SDSId -> Dynamic -> Invalidate p -> NEvent

class Readable v where
    readR :: v m p r w -> p -> m (ReadResult m p r w)
data ReadResult m p r w where
    Read    :: Typeable p => r -> ReadResult m p r w
    Reading :: (Typeable p, Readable v) => v m p r w -> ReadResult m p r w

observe :: (Monad m, Observable v) => v m p r w -> p -> String -> m () -> PViewT m ()
observe v p oid ohnd = execWriterT (genreqs v p)
    >>= \rs->modify ([NRequest sdsId oid ohnd dyn|(sdsId, dyn)<-rs]++)

class Nameable v => Observable v where
    genreqs :: v m p r w -> p -> WriterT [(SDSId, Dynamic)] (PViewT m) ()

writeF :: Monad m => (v m p r w -> p -> w -> WriterT [NEvent] (PViewT m) res) -> v m p r w -> p -> w -> PViewT m res
writeF wfun v p w = runWriterT (wfun v p w) >>= \(r, n)->trigger (reverse n) >> pure r
  where
    trigger :: Monad m => [NEvent] -> PViewT m ()
    trigger es = do
        rs <- get
        skips <- foldM (\skips event->foldM (match event) skips rs) [] es
        modify $ filter (\(NRequest _ oid _ _)->oid `notElem` skips)
      where
        match :: (MonadState [NRequest m] (PViewT m), Monad m) => NEvent -> [String] -> NRequest m -> PViewT m [String]
        match (NEvent eventSdsId _ inval) skips (NRequest reqSdsId oid ohnd dyn)
            | reqSdsId == eventSdsId && oid `notElem` skips
                = case fromDynamic dyn of
                    Just p' -> when (inval p') (lift ohnd) >> pure (oid:skips)
                    Nothing -> pure skips
        match _ skips _ = pure skips

write :: (Monad m, Writeable v) => v m p r w -> p -> w -> PViewT m (WriteResult m p r w)
write = writeF writeR

returnEA :: (Monad m, Typeable p, Typeable a) => SDSId -> a -> Invalidate p -> WriterT [NEvent] m (Invalidate p)
returnEA sid p inval = tell [NEvent sid (toDyn p) inval] >> pure inval

returnE :: (Monad m, Typeable p) => SDSId -> p -> WriteResult m p2 r w -> WriterT [NEvent] (PViewT m) (WriteResult m p2 r w)
returnE sid p (Written inval) = Written <$> returnEA sid p inval
returnE _ _ (Writing v) = pure $ Writing v

class Nameable (v :: (Type -> Type) -> Type -> Type -> Type -> Type) where
    sdsname :: v m p r w -> SDSId
class Nameable v => Writeable v where
    writeR :: v m p r w -> p -> w -> WriterT [NEvent] (PViewT m) (WriteResult m p r w)
data WriteResult m p r w where
    Written :: (Monad m, Typeable p) => Invalidate p -> WriteResult m p r w
    Writing :: (Writeable v, Monad m, Typeable p) => v m p r w -> WriteResult m p r w

getShare :: (Monad m, Readable v) => v m p r w -> p -> m r
getShare v p = readR v p >>= \case
    Read r -> pure r
    Reading nv -> getShare nv p

setShare :: (Monad m, Writeable v) => v m p r w -> p -> w -> PViewT m ()
setShare v p w = write v p w >>= \case
    Written _ -> pure ()
    Writing nv -> setShare nv p w

updateR :: (Readable v, Writeable v, Monad m) => v m p r w -> p -> (r -> w) -> PViewT m (UpdateResult m p r w)
updateR v p f = lift (readR v p) >>= \case
    Reading vr -> updateR (VRWPair vr v) p f
    Read r -> let w = f r in write v p w >>= \case
        Written _ -> pure $ Updated w
        Writing vw -> updateR (VRWPair (constShare r) vw) p (const w)

data VRWPair vr vw m p r w where
    VRWPair :: (Typeable p, Monad m) => vr m p r w -> vw m p r w -> VRWPair vr vw m p r w
instance Nameable vw => Nameable (VRWPair vr vw) where
    sdsname (VRWPair _ vw) = sdsname vw
instance Writeable vw => Writeable (VRWPair vr vw) where
    writeR (VRWPair _ vw) = writeR vw
instance Observable vw => Observable (VRWPair vr vw) where
    genreqs (VRWPair _ vw) = genreqs vw
instance Readable vr => Readable (VRWPair vr vw) where
    readR (VRWPair vr _) = readR vr

data VRSource m p r w where
    VRS     :: (Typeable p, Monad m) => (p -> m r) -> VRSource m p r w
data VWSource m p r w where
    VWS     :: (Typeable p, Monad m) => SDSId -> (p -> w -> m (Invalidate p)) -> VWSource m p r w
newtype VSource m p r w = VS (VRSource m p r w, VWSource m p r w)

constShare :: (Typeable p, Monad m) => r -> VRSource m p r w
constShare r = VRS $ \_->pure r

nullShare :: (Typeable p, Monad m) => Invalidate p -> VWSource m p r w
nullShare inval = VWS "null" $ \_ _->pure inval

unitShare :: (Typeable p, Monad m) => Invalidate p -> VSource m p () ()
unitShare inval = VS (constShare (), nullShare inval)

instance Readable VRSource where
    readR v@VRS{} p = Read <$> readA v p
instance Readable VSource where
    readR (VS (v@VRS{}, _)) p = Read <$> readA v p
instance Observable VWSource where
    genreqs v@VWS{} p = tell [(sdsname v, toDyn p)]
instance Observable VSource where
    genreqs (VS (_, vw)) = genreqs vw
instance Nameable VWSource where
    sdsname (VWS sid _) = sid
instance Writeable VWSource where
    writeR v@VWS{} p w = Written <$> writeA v p w
instance Nameable VSource where
    sdsname (VS (_, vw)) = sdsname vw
instance Writeable VSource where
    writeR (VS (_, v@VWS{})) p w = Written <$> writeA v p w

class ReadableA v where
    readA :: v m p r w -> p -> m r
class WriteableA v where
    writeA :: Monad m => v m p r w -> p -> w -> WriterT [NEvent] (PViewT m) (Invalidate p)

instance ReadableA VRSource where
    readA (VRS r) = r
instance ReadableA VSource where
    readA (VS (vr, _)) = readA vr
instance ReadableA vr => ReadableA (VRWPair vr vw) where
    readA (VRWPair vr _) = readA vr
instance WriteableA VWSource where
    writeA (VWS sid wfun) p w = lift (lift $ wfun p w) >>= returnEA sid p
instance WriteableA VSource where
    writeA (VS (_, vw)) = writeA vw
instance WriteableA vw => WriteableA (VRWPair vr vw) where
    writeA (VRWPair _ vw) = writeA vw


class Updateable v where
    update :: (Monad m, Typeable p) => v m p r w -> p -> (r -> w) -> PViewT m (UpdateResult m p r w)
data UpdateResult m p r w where
    Updated :: Typeable p => w -> UpdateResult m p r w
    Updating :: (Typeable p, Updateable v) => v m p r w -> UpdateResult m p r w

instance Updateable VSource where
    update v p f = lift (readA v p) >>= \r->let w = f r in writeF writeA v p w >> pure (Updated w)
instance (ReadableA vr, WriteableA vw) => Updateable (VRWPair vr vw) where
    update v@(VRWPair _ _) p f = lift (readA v p) >>= \r->let w = f r in writeF writeA v p w >> pure (Updated w)
