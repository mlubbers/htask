module ACSDS.Lens where

import Data.Functor
import Data.Dynamic
import Data.Maybe
import Control.Monad.Writer

import ACSDS

data VLens v m p r w where
    VLens   :: (Typeable p, Monad m) => String -> v m p r1 w1 -> Lens r1 w1 r w -> VLens v m p r w
data VBilens v m p r w where
    VBilens :: (Typeable p, Monad m) => String -> v m p r1 w1 -> Bilens r1 w1 r w -> VBilens v m p r w
data Lens r1 w1 r2 w2 = Lens { getl :: r1 -> r2 , putl :: r1 -> w2 -> w1 }
data Bilens r1 w1 r2 w2 = Bilens { getlb :: r1 -> r2 , putlb :: w2 -> w1 }
instance Readable v => Readable (VLens v) where
    readR (VLens n v lens) p = readR v p <&> \case
        Read r -> Read $ getl lens r
        Reading v' -> Reading $ VLens n v' lens
instance Readable v => Readable (VBilens v) where
    readR (VBilens n v lens) p = readR v p <&> \case
        Read r -> Read $ getlb lens r
        Reading v' -> Reading $ VBilens n v' lens
instance Observable v => Observable (VLens v) where
    genreqs (VLens _ v _) = genreqs v
instance Observable v => Observable (VBilens v) where
    genreqs (VBilens _ v _) = genreqs v
instance Nameable v => Nameable (VLens v) where
    sdsname (VLens n v _) = sdsname v ++ "(" ++ n ++ ")"
instance (Readable v, Writeable v) => Writeable (VLens v) where
    writeR (VLens n v lens) p w = lift (lift $ readR v p) >>= \case
        Reading vr -> pure $ Writing $ VLens n (VRWPair vr v) lens
        Read r -> writeR (VBilens n v $ Bilens (getl lens) (putl lens r)) p w
instance Nameable v => Nameable (VBilens v) where
    sdsname (VBilens n v _) = sdsname v ++ "(b|" ++ n ++ ")"
instance Writeable v => Writeable (VBilens v) where
    writeR (VBilens n v lens) p w = writeR v p (putlb lens w) <&> \case
        Writing vw -> Writing $ VBilens n (VRWPair v vw) lens
        Written inval -> Written inval

instance ReadableA v => ReadableA (VLens v) where
    readA (VLens _ v lens) p = readA v p <&> getl lens
instance ReadableA v => ReadableA (VBilens v) where
    readA (VBilens _ v lens) p = readA v p <&> getlb lens

instance (ReadableA v, WriteableA v) => WriteableA (VLens v) where
    writeA (VLens _ v lens) p w = lift (lift $ readA v p) >>= writeA v p . flip (putl lens) w
instance (WriteableA v) => WriteableA (VBilens v) where
    writeA (VBilens _ v lens) p w = writeA v p $ putlb lens w

toReadOnly :: (Monad m, Typeable p) => v m p r r -> VLens v m p r ()
toReadOnly sds = VLens "toReadOnly" sds $ Lens { getl = id, putl = \w ()->w }

removeMaybe :: (Monad m, Typeable p) => a -> v m p (Maybe a) (Maybe a) -> VBilens v m p a a
removeMaybe alt sds = VBilens "removeMaybe" sds $ Bilens { getlb = fromMaybe alt, putlb = Just }
