module ACSDS.Pair where

import Data.Dynamic

import ACSDS

data VPair vl vr m p r w where
    VPair   :: (Typeable p1, Typeable p2, Monad m) =>
        vl p1 r1 w1 -> vr p2 r2 w2 -> VPair vl vr (p1, p2) (r1, r2) (w1, w2)
instance (Readable vl, Readable vr) => Readable (VPair vl vr) where
    readR (VPair vl vr) (pl, pr) = mkRes <$> readR vl pl <*> readR vr pr
      where
        mkRes (Read l) (Read r) = Read (l, r)
        mkRes (Read l) (Reading r) = Reading $ VPair (constShare l) r
        mkRes (Reading l) (Read r) = Reading $ VPair l (constShare r)
        mkRes (Reading l) (Reading r) = Reading $ VPair l r
instance (Observable vl, Observable vr) => Observable (VPair vl vr) where
    genreqs (VPair lv rv) (pl, pr) = genreqs lv pl >> genreqs rv pr
instance (Writeable vl, Writeable vr) => Writeable (VPair vl vr) where
    writeR (VPair vl vr) (pl, pr) (wl, wr) = mkRes <$> writeR vl pl wl <*> writeR vr pr wr
      where
        mkRes (Written l) (Written r) = Written $ \(pl', pr')->l pl' || r pr'
        mkRes (Written l) (Writing r) = Writing $ VPair (nullShare l) r
        mkRes (Writing l) (Written r) = Writing $ VPair l (nullShare r)
        mkRes (Writing l) (Writing r) = Writing $ VPair l r
