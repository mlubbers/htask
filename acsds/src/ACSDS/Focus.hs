module ACSDS.Focus where

import Data.Dynamic
import Data.Functor
import Control.Monad.Writer

import ACSDS

data VFocus v m p r w where
    VFocus  :: (Eq p1, Typeable p1, Typeable p2, Monad m) => String -> v m p1 r1 w1 -> PLens p1 p2 r1 w1 r w -> VFocus v m p2 r w
data VBifocus v m p r w where
    VBifocus  :: (Eq p1, Typeable p1, Typeable p2, Monad m) => String -> v m p1 r1 w1 -> BiPLens p1 p2 r1 w1 r w -> VBifocus v m p2 r w

data PLens p1 p2 r1 w1 r2 w2 = PLens { paramp :: p2 -> p1, getp :: p2 -> r1 -> r2 , putp :: p2 -> r1 -> w2 -> (w1, Invalidate p2) }
data BiPLens p1 p2 r1 w1 r2 w2 = BiPLens { parampb :: p2 -> p1, getpb :: p2 -> r1 -> r2 , putpb :: p2 -> w2 -> (w1, Invalidate p2) }
instance Readable v => Readable (VFocus v) where
    readR (VFocus n v lens) p2 = let p1 = paramp lens p2 in readR v p1 <&> \case
        Read r -> Read $ getp lens p2 r
        Reading v' -> Reading $ VFocus n v' lens
instance Readable v => Readable (VBifocus v) where
    readR (VBifocus n v lens) p2 = let p1 = parampb lens p2 in readR v p1 <&> \case
        Read r -> Read $ getpb lens p2 r
        Reading v' -> Reading $ VBifocus n v' lens
instance Observable v => Observable (VFocus v) where
    genreqs ov@(VFocus _ v lens) p2 = let p1 = paramp lens p2 in tell [(sdsname ov, toDyn p2), (sdsname v, toDyn p1)] >> genreqs v p1
instance Observable v => Observable (VBifocus v) where
    genreqs ov@(VBifocus _ v lens) p2 = let p1 = parampb lens p2 in tell [(sdsname ov, toDyn p2), (sdsname v, toDyn p1)] >> genreqs v p1
instance Nameable v => Nameable (VFocus v) where
    sdsname (VFocus n v _) = sdsname v ++ "[" ++ n ++ "]"
instance (Readable v, Writeable v) => Writeable (VFocus v) where
    writeR (VFocus n v lens) p2 w =
        let p1 = paramp lens p2
        in lift (lift $ readR v p1) >>= \case
            Reading vr -> pure $ Writing $ VFocus n (VRWPair vr v) lens
            Read r -> writeR (VBifocus n v $ BiPLens (paramp lens) (getp lens) (\p'->putp lens p' r)) p2 w
instance Nameable v => Nameable (VBifocus v) where
    sdsname (VBifocus n v _) = sdsname v ++ "[b|" ++ n ++ "]"
instance Writeable v => Writeable (VBifocus v) where
    writeR sds@(VBifocus n v lens) p2 w =
        let (s', invall) = putpb lens p2 w
            p1 = parampb lens p2
        in writeR v p1 s' >>= \case
            Writing vw -> pure $ Writing $ VBifocus n (VRWPair v vw) lens
            Written invalw -> returnE (sdsname sds) p1 $ Written $ \p2'->
                let p1' = parampb lens p2'
                in if p1 == p1' then invall p2' else invalw p1'

sdsTranslate :: (Eq p1, Typeable p1, Typeable p2, Monad m) => String -> (p2 -> p1) -> v m p1 r w -> VBifocus v m p2 r w
sdsTranslate n pfun sds = VBifocus n sds $ BiPLens pfun (const id) (\_ w->(w, const True))

sdsFocus :: (Show p1, Eq p1, Typeable p1, Typeable p2, Monad m) => p1 -> v m p1 r w -> VBifocus v m p2 r w
sdsFocus p = sdsTranslate (show p) (const p)
