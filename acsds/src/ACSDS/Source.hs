module ACSDS.Source where

import ACSDS
import ACSDS.Lens
import Data.Time
import System.Random
import System.Directory
import Control.Monad.IO.Class

randomShare :: MonadIO m => Random a => VRSource m () a ()
randomShare = VRS $ \_->liftIO randomIO

currentDateTimeShare :: MonadIO m => VSource m () UTCTime UTCTime
currentDateTimeShare = VS (VRS $ \_->liftIO getCurrentTime, VWS "currentDateTimeShare" $ \_ _->pure (const True))

currentTimeShare :: MonadIO m => VLens VSource m () DiffTime DiffTime
currentTimeShare = VLens "currentTime" currentDateTimeShare $ Lens { getl = utctDayTime, putl = const }

currentDateShare :: MonadIO m => VLens VSource m () Day Day
currentDateShare = VLens "currentDate" currentDateTimeShare $ Lens { getl = utctDay, putl = const }

directoryShare :: MonadIO m => VRSource m FilePath [FilePath] ()
directoryShare = VRS $ liftIO . getDirectoryContents

fileShare :: MonadIO m => VSource m FilePath String String
fileShare = VS
    ( VRS $ \fp->liftIO (readFile fp >>= \s->length s `seq` pure s)
    , VWS "fileShare" $ \p w->liftIO (writeFile p w) >> pure (p==)
    )
