module Main where

import Data.Time

import Task
--import Task.Eval
import Task.Basic
import Task.Value
import Task.Editor
import Task.Eval.Brick
import Task.Eval.Repl
import Task.Eval.Wai
import Task.Parallel
import Task.SDS
import Task.Sequential

--import ACSDS
import ACSDS.Source
import ACSDS.Focus
--import ACSDS.Lens
--import Data.Time

--main :: IO ()
--main = doTaskBrick $
--        edit "Blurp" -&&-
--        (edit "Mart" >>* [OnValue $ ifValue (=="Mart Lubbers") $ \v->view $ "Hello " ++ v])

--main :: IO ()
--main = doTaskRepl (stopWhenStable $ yield 42)
--main = doTaskBrick helloWorld
--main = doTaskBrick helloWorldConfirm
--main = doTaskReplExit helloWorldConfirm
--main = doTaskBrick sharedEditor
--main = doTaskRepl sharedEditor
--main = doTaskBrick viewTime
--main = doTaskRepl viewTime
--main = doTaskBrick asyncExample
--main = doTaskReplExit asyncExample
--main = doTaskWarp [WebTask "hello" $ view 42]
--main = doTaskWai helloWorld
--main = doTasksWai
--    [ WebTask "time" viewTime
--    , WebTask "hello" helloWorld
--    , WebTask "hello2" helloWorldConfirm
--    , WebTask "seditor" sharedEditor
--    ]
main = doTaskBrick examples
--main = doTaskRepl examples
--main = doTaskWai examples!

examples :: UIProvider u => Task u ()
examples = view "Pick a task" >>*
    [ OnAction "Hello world!" $ always $ helloWorld @! ()
    , OnAction "Hello world! (advanced)" $ always $ helloWorldConfirm @! ()
    , OnAction "View the time" $ always $ viewTime @! ()
    , OnAction "Work on a shared editor" $ always $ sharedEditor @! ()
    , OnAction "Sequence of steps" $ always $ stepSequence @! ()
    ] >?| examples

viewTime :: UIProvider u => Task u UTCTime
viewTime = viewSds currentDateTimeShare <<@ panel (Just "Now")

helloWorld :: UIProvider u => Task u String
helloWorld = enter >>* [OnValue $ withValue $ \v->Just $ view $ "Hello " ++ v ++ "!"]

helloWorldConfirm :: UIProvider u => Task u String
helloWorldConfirm = (enter <<@ panel (Just "Name")) >>*
    [ OnAction "Cancel" $ always $ view "Cancelled"
    , OnValue $ ifValue (=="Mart") $ \v->view $ "Hello " ++ v ++ "!"
    , OnAction "Palindrome" $ ifValue (\v->reverse v == v) $ \v->view $ "!Hello " ++ v ++ " olleH!"
    , OnAction "Confirm" $ withValue $ \v->Just $ view $ "Hello " ++ v ++ "!"
    ]

sharedEditor :: UIProvider u => Task u (Int, Int)
sharedEditor = withShared (42 :: Int) $ \sds->
         (editSds sds <<@ panel (Just "editor1"))
    -&&- (editSds sds <<@ panel (Just "editor2"))
    -||  (viewSds sds <<@ panel (Just "viewer"))
    <<@ vertical

stepSequence :: UIProvider u => Task u [()]
stepSequence = foldr (\a b->view ("step " ++ show a) >>* [OnAction "Next" $ always b]) (yield []) [0..10 :: Int]
--main :: IO ()
--main = doTaskBrick $
--        taskIO (putStrLn "Starting up")
--    >-|      sdsSet sds ()
--        -&&- watch sds
--        -&&- sdsSet sds ()
--        -&&- rfile
--        -&&- withShared (42 :: Int, True) (\lsds->sdsSet lsds (5, False) >-| sdsGet lsds >>- taskIO . print)
----    ((taskIO $ putStrLn "Hello World!") >>* [OnValue $ always $ taskIO $ putStrLn "Hello You!"])
----    -&&- (taskIO $ putStrLn "Bork")
----    -&&- ((sdsGet randomShare :: Task Int) >>- taskIO . putStrLn . show)
----    -&&- (sdsGet currentDateTimeShare >>- taskIO . putStrLn . show)
----    -&&- (sdsGet currentTimeShare >>- taskIO . putStrLn . show . timeToTimeOfDay)
----    -&&- (sdsGet currentDateShare >>- taskIO . putStrLn . showGregorian)
--  where
--    sds = unitShare $ const True
--
--    rfile = sdsGet (sdsFocus "/home/mrl/url.py" fileShare)
--        >>- taskIO . putStrLn . head . lines
