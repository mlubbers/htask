module Task.Eval.Brick (doTaskBrick, BrickUI) where

import Brick
import Brick.Widgets.Border
import Brick.Widgets.Edit
import Brick.Forms
import Brick.BChan
import Brick.Widgets.Center
import qualified Brick.Widgets.List as L
import Brick.Widgets.Dialog
import qualified Brick.Focus as F

import Graphics.Vty.Attributes
import Graphics.Vty.Input
import Graphics.Vty

import qualified Data.Vector as V

import Task
import Task.Eval

import Control.Concurrent

import Control.Monad.State
import qualified Data.Map as DM
import Data.Maybe

data BrickUI
    = BrickEditor (Editor String TaskId)
    | BrickDialog (Dialog TaskId)
instance Show BrickUI
  where
    show (BrickEditor ed) = "BrickEditor (text=" ++ unlines (getEditContents ed) ++ ")"
    show (BrickDialog dia) = "BrickDialog (selected=" ++ show (dialogSelectedIndex dia) ++ ")"
instance UIProvider BrickUI
  where
    provideEditor tid v = BrickEditor $ editor tid (Just 1) v
    provideStep _ buttons _ = BrickDialog $ dialog Nothing (Just (0, zipWith mkButton buttons [0..])) 999

mkButton :: (Bool, String) -> Int -> ([Char], Int)
mkButton (enabled, buttontext) idx = (if enabled then buttontext ++ "(" ++ show idx ++ ")" else "", idx)

data AppSt = AppSt
    { uis        :: DM.Map InstanceId (TaskUI BrickUI)
    , taskState  :: TaskState BrickUI
    , focusRing  :: F.FocusRing TaskId
    , focus      :: TaskId
    , logging    :: L.GenericList TaskId V.Vector String
    }
type AppEvent = ()

doTaskBrick :: Task BrickUI a -> IO ()
doTaskBrick task = do
    eventChan <- newBChan 10

    void $ forkIO $ forever $ do
        writeBChan eventChan ()
        threadDelay $ 60*1000*1000

    let buildVty = mkVty defaultConfig
    initialVty <- buildVty
    (_, st, _lg) <- runTaskM (initInstance task) initialTaskState
    void $ customMain initialVty buildVty (Just eventChan) app (initialState st)
  where
    initialState st = AppSt
        { uis=DM.empty
        , taskState=st
        , focusRing=F.focusRing [-1]
        , focus= -1
        , logging=L.list (-1) V.empty 1
        }

    app :: App AppSt AppEvent TaskId
    app = App
        { appDraw         = drawUI
        , appChooseCursor = F.focusRingCursor focusRing
        , appHandleEvent  = handleEvent
        , appStartEvent   = eventLoop
        , appAttrMap      = theAttrMap
        }

theAttrMap :: AppSt -> AttrMap
theAttrMap _ = attrMap defAttr
  [ (editAttr, white `on` black)
  , (editFocusedAttr, black `on` yellow)
  , (invalidFormInputAttr, white `on` red)
  , (focusedFormInputAttr, black `on` yellow)
  , (dialogAttr, white `on` black)
  , (buttonAttr, black `on` white)
  , (buttonSelectedAttr, bg yellow)
  , (L.listAttr, white `on` black)
  , (L.listSelectedAttr, black `on` yellow)
  ]

handleEvent :: AppSt -> BrickEvent TaskId AppEvent -> EventM TaskId (Next AppSt)
handleEvent s (VtyEvent e) = case e of
    EvLostFocus -> continueWithoutRedraw s
    EvGainedFocus -> continueWithoutRedraw s
    EvResize _ _ -> continue s
    (EvKey KEsc []) -> halt s
    (EvKey (KChar 'c') [MCtrl]) -> halt s
    (EvKey (KChar 'l') [MCtrl]) -> continue s
    (EvKey (KChar '\t') []) -> continue $ alterRing F.focusNext s
    (EvKey KBackTab []) -> continue $ alterRing F.focusPrev s
    (EvKey key _) -> do
        case F.focusGetCurrent (focusRing s) of
            Nothing -> continueWithoutRedraw s
            Just (-1) -> do
                l <- L.handleListEventVi L.handleListEvent e (logging s)
                continue $ s { logging=l }
            Just tid -> case DM.lookup tid $ getUI s of
                Nothing -> continueWithoutRedraw s
                Just (BrickEditor ed) -> do
                    ed' <- handleEditorEvent e ed
                    let s'  = withUIState (DM.insert tid $ BrickEditor ed')
                            $ s { uis = DM.adjust (setUI tid $ BrickEditor ed') 0 $ uis s }
                    case key of
                        KEnter -> do
                            let s'' = withTaskState (queueEventTaskState (EditEvent tid (unlines $ getEditContents ed'))) s'
                            continue =<< eventLoop s''
                        _ -> continue s'
                Just (BrickDialog dia) -> do
                    dia' <- handleDialogEvent e dia
                    let s'  = withUIState (DM.insert tid $ BrickDialog dia')
                            $ s { uis = DM.adjust (setUI tid $ BrickDialog dia') 0 $ uis s }
                    case key of
                        KEnter -> do
                            let s'' = withTaskState (queueEventTaskState (ButtonEvent tid $ fromMaybe (-1) $ dialogSelection dia')) s'
                            continue =<< eventLoop s''
                        _ -> continue s'
    _ -> continueWithoutRedraw s
  where
    withUIState :: (DM.Map TaskId BrickUI -> DM.Map TaskId BrickUI) -> AppSt -> AppSt
    withUIState f = withUIStates $ DM.alter (Just . f . fromMaybe DM.empty) 0

    withUIStates :: (DM.Map InstanceId (DM.Map TaskId BrickUI) -> DM.Map InstanceId (DM.Map TaskId BrickUI)) -> AppSt -> AppSt
    withUIStates f = withTaskState $ \ts->ts { uistates=f $ uistates ts }

    withTaskState :: (TaskState BrickUI -> TaskState BrickUI) -> AppSt -> AppSt
    withTaskState f st = st { taskState = f $ taskState st }

    getUI :: AppSt -> DM.Map TaskId BrickUI
    getUI = DM.findWithDefault DM.empty 0 . uistates . taskState

    alterRing fun st =
        let nfr = fun (focusRing st)
        in  st { focus = fromMaybe 0 $ F.focusGetCurrent nfr, focusRing=nfr }
handleEvent s (AppEvent _) = continue =<< eventLoop s
handleEvent s MouseDown{} = continue s
handleEvent s MouseUp{} = continue s

eventLoop :: AppSt -> EventM TaskId AppSt
eventLoop s = do
    (ui, ts, lg) <- liftIO $ taskLoop (uis s) (taskState s)
    pure $ s
        { logging=L.list (-1) (let l = L.listElements (logging s) V.++ V.fromList lg in V.drop (V.length l-20) l) 1
        , uis=ui
        , taskState=ts
        , focusRing=F.focusSetCurrent (focus s) $ F.focusRing $ -1:taskUITaskIds (getInstanceZero ui)
        }

drawUI :: AppSt -> [Widget TaskId]
drawUI st = [
        hCenter (str $ "hTask --- arrows to move within button dialog, tab to move between elements\n"
                    ++ "current focus: " ++ show (F.focusGetCurrent (focusRing st)))
    <=> hBorder
    <=> border (L.renderList (\_ e->str e) (F.focusGetCurrent (focusRing st) == Just (-1)) $ logging st)
    <=> mkUI st (getInstanceZero $ uis st)
    ]

mkUI :: AppSt -> TaskUI BrickUI -> Widget TaskId
mkUI _ UINone = str "none"
mkUI _ (UIView s) = str s
mkUI st (UIStep _ [] ui _) = mkUI st ui
mkUI st (UIStep tid _ ui (BrickDialog dia)) = withBorder ("(" ++ show tid ++ ")") $ renderDialog dia (mkUI st ui)
mkUI _ UIStep{} = str "Stored BrickUI not a dialog"
mkUI st (UIEditor tid _ (BrickEditor ed)) = withBorder ("(" ++ show tid ++ ")") $ renderEditor (str . unlines) inFocus ed
  where inFocus = F.focusGetCurrent (focusRing st) == Just tid
mkUI _ UIEditor{} = str "Stored BrickUI not an editor"
mkUI st (UIHorizontal u) = foldr ((<+>) . mkUI st) emptyWidget u
mkUI st (UIVertical u) = foldr ((<=>) . mkUI st) emptyWidget u
mkUI st (UIPanel Nothing u) = border $ mkUI st u
mkUI st (UIPanel (Just title) u) = withBorder title $ mkUI st u

withBorder :: String -> Widget n -> Widget n
withBorder title = borderWithLabel (str title) . padRight (Pad $ textWidth title)

getInstanceZero :: Show u => DM.Map InstanceId (TaskUI u) -> TaskUI u
getInstanceZero m = fromMaybe (UIView $ "No instance 0 in: " ++ show m) $ DM.lookup 0 m
