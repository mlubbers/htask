module Main where

import Test.Framework (defaultMain, testGroup)
import Test.Framework.Providers.QuickCheck2 (testProperty)

import Test.QuickCheck
import Test.QuickCheck.Checkers
import Test.QuickCheck.Classes

import Task.Value

instance Arbitrary a => Arbitrary (TaskValue a) where
  arbitrary = oneof [pure NoValue, Unstable <$> arbitrary, Stable <$> arbitrary]
instance Eq a => EqProp (TaskValue a) where
  (=-=) = eq

main = defaultMain tests

tests =
    [ testBatch "functor laws" $ functor x
    , testBatch "applicative laws" $ applicative x
    , testBatch "alternative laws" $ alternative (Stable (42 :: Int))
    , testBatch "monad laws" $ monad x
    ]
  where
    x :: TaskValue (Int, Char, Bool)
    x = Stable (42, 'c', True)

    testBatch name = testGroup name . map (uncurry testProperty) . unbatch
