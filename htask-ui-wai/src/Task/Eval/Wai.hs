{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-orphans #-}
module Task.Eval.Wai (doTaskWai, doTasksWai, WebTask(..), WaiUI) where

import GHC.Generics
import Data.Aeson
import Data.Foldable
import Control.Concurrent.MVar
import Control.Exception
import Data.String
import qualified Data.Text as T
import qualified Data.ByteString.Lazy.Char8 as L

import qualified Data.Map as DM
import Network.HTTP.Types
import qualified Network.Wai as W
import qualified Network.Wai.Middleware.Static as Static
import Network.Wai.Handler.Warp (run)
import qualified Network.Wai.Handler.WebSockets as WWS
import qualified Network.WebSockets as WS

import Task
import Task.Eval

type WaiUI = ()
data WebTask = forall a. WebTask { path :: String, task :: Task WaiUI a }
instance ToJSON (TaskUI WaiUI)
instance ToJSON MessageSend
instance FromJSON MessageRecv

data MessageSend
    = Update   { newUI :: TaskUI WaiUI }
    | ErrorMsg { errorMsg :: String }
  deriving Generic
data MessageRecv
    = Init   { entrypoint :: String }
    | Edit   { editorId :: TaskId, editorValue :: String }
    | Button { buttonId :: TaskId, buttonValue :: Int }
  deriving (Generic, Show)

type WaiState = (DM.Map InstanceId (TaskUI WaiUI), TaskState WaiUI)

doTaskWai :: Task WaiUI a -> IO ()
doTaskWai t = doTasksWai [WebTask "" t]

doTasksWai :: [WebTask] -> IO ()
doTasksWai webtasks = do
    stateMVar <- newMVar (DM.empty, initialTaskState)
    cstateMVar <- newMVar DM.empty
    putStrLn "Server running on http://localhost:8080"
    putStrLn "available tasks:"
    sequence_ [putStrLn $ "\t- " ++ ('/':path w) | w<-webtasks]
    run 8080 $ WWS.websocketsOr WS.defaultConnectionOptions (wsapp stateMVar cstateMVar)
             $ Static.staticPolicy (Static.noDots Static.>-> Static.addBase "frontend") app
  where
    app :: W.Application
    app request respond = case (W.requestMethod request, W.pathInfo request) of
        -- For single or no component paths we just return the main file
        ("GET", s) | length s <= 1 -> respond $ W.responseFile status200 [("Content-Type", "text/html")] "frontend/htask.html" Nothing
        -- Otherwise it's always an error
        (method, reqpath) -> respond $ W.responseLBS status404 [("Content-Type", "text/plain")] $ L.fromStrict method <> ": " <> fromString (T.unpack $ T.intercalate "/" reqpath)

    wsapp :: MVar WaiState -> MVar (DM.Map InstanceId WS.Connection) -> WS.ServerApp
    wsapp stateMVar cstateMVar pending = do
        conn <- WS.acceptRequest pending
        WS.withPingThread conn 30 (return ()) $ do
            msg <- WS.receiveData conn
            case decode msg of
                Nothing -> sendMsg conn $ ErrorMsg "invalid message"
                Just (Init ep) -> case find (\(WebTask i _)->i == fromString (drop 1 ep)) webtasks of
                    Nothing -> sendMsg conn $ ErrorMsg $ "task " ++ ep ++ " doesn't exist"
                    Just (WebTask _ t) -> do
                        (iid, _lg) <- runTMSt $ initInstance t
                        -- Add the client to the connection map
                        modifyMVar_ cstateMVar $ pure . DM.insert iid conn
                        putStrLn ("Instance " ++ show iid ++ " for task " ++ drop 1 ep)
                        finally (handleClient conn iid) $ disconnect iid
                Just _ -> sendMsg conn $ ErrorMsg "init first"
      where
        sendMsg :: WS.Connection -> MessageSend -> IO ()
        sendMsg conn = WS.sendTextData conn . encode . toJSON

        runTMSt :: TaskMonad WaiUI a -> IO (a, [String])
        runTMSt m = modifyMVar stateMVar $ \(ui, st)->(\(a, st', lg)->((ui, st'), (a, lg))) <$> runTaskM m st

        disconnect :: InstanceId -> IO ()
        disconnect iid = do
            ((), _lg) <- runTMSt $ deinitInstance iid
            -- Remove the client from the connection map
            modifyMVar_ cstateMVar $ pure . DM.delete iid

        handleClient :: WS.Connection -> InstanceId -> IO ()
        handleClient conn iid = do
            -- Run the task loop and yield the updated UIs
            (uis, _lg) <- modifyMVar stateMVar $ \(ui, st)->do
                (ui', st', lg) <- taskLoop DM.empty st
                pure ((ui' `DM.union` ui, st'), (DM.toList ui', lg))
            -- Update the UIs in the browser
            mapM_ (uncurry sendTaskUI) uis
            -- Wait for a new message from the client
            msg <- WS.receiveData conn
            case decode msg of
                Nothing -> sendMsg conn $ ErrorMsg $ "invalid message: " ++ show msg
                Just (Init t) -> sendMsg conn $ ErrorMsg $ "already initialised: " ++ show t
                Just e@(Edit tid v) -> do
                    putStrLn $ "instance " ++ show iid ++ ": " ++ show e
                    (_, _lg) <- runTMSt $ queueEventForInstance iid (EditEvent tid v)
                    handleClient conn iid
                Just e@(Button tid idx) -> do
                    putStrLn $ "instance " ++ show iid ++ ": " ++ show e
                    (_, _lg) <- runTMSt $ queueEventForInstance iid (ButtonEvent tid idx)
                    handleClient conn iid
          where
            sendTaskUI :: InstanceId -> TaskUI WaiUI -> IO ()
            sendTaskUI ciid ui = modifyMVar_ cstateMVar $ \s->case DM.lookup ciid s of
                Nothing -> pure s
                Just iconn -> sendMsg iconn (Update ui) >> pure s
