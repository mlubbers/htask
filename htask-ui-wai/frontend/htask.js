function renderUI(socket, ui, el) {
	switch (ui.tag) {
		case "UINone":
			el.appendChild(document.createTextNode("none"));
			break;
		case "UIStep":
			var table = document.createElement("table");
			var hrow = document.createElement("tr");
			var hcell = document.createElement("td");
			hcell.colSpan = ui.stepButtons.length;
			renderUI(socket, ui.stepChildUI, hcell);
			hrow.appendChild(hcell);
			table.appendChild(hrow);
			var row = document.createElement("tr");
			ui.stepButtons.forEach(function (button, idx) {
				var cell = document.createElement("td");
				var btn = document.createElement("button");
				if (button[0]) {
					btn.onclick = function () {
						socket.send(JSON.stringify(
							{ "buttonId": ui.stepId
							, "buttonValue": idx
							, "tag": "Button"
							}));
					};
				} else {
					btn.disabled = true;
				}
				btn.appendChild(document.createTextNode(button[1]));
				row.appendChild(cell);
				cell.appendChild(btn);
			});
			table.appendChild(row);
			el.appendChild(table);
			break;
		case "UIHorizontal":
			var table = document.createElement("table");
			var row = document.createElement("tr");
			ui.horizontalChildUIs.forEach(function (uic) {
				var cell = document.createElement("td");
				renderUI(socket, uic, cell);
				row.appendChild(cell);
			});
			table.appendChild(row);
			el.appendChild(table);
			break;
		case "UIVertical":
			var table = document.createElement("table");
			ui.verticalChildUIs.forEach(function (uic) {
				var row = document.createElement("tr");
				var cell = document.createElement("td");
				renderUI(socket, uic, cell);
				row.appendChild(cell);
				table.appendChild(row);
			});
			el.appendChild(table);
			break;
		case "UIView":
			el.appendChild(document.createTextNode(ui.viewValue));
			break;
		case "UIPanel":
			if (ui.panelTitle == null) {
				renderUI(socket, ui.panelChildUI, el);
			} else {
				var table = document.createElement("table");
				var hrow = document.createElement("tr");
				var hcell = document.createElement("th");
				hcell.appendChild(document.createTextNode(ui.panelTitle));
				hrow.appendChild(hcell);
				table.appendChild(hrow);
				var row = document.createElement("tr");
				var cell = document.createElement("td");
				renderUI(socket, ui.panelChildUI, cell);
				row.appendChild(cell);
				table.appendChild(row);
				el.appendChild(table);
			}
			break;
		case "UIEditor":
			var child = document.createElement("input");
			child.value = ui.editorValue;
			child.onchange = function () {
				socket.send(JSON.stringify(
					{ "editorId": ui.editorId
					, "editorValue": child.value
					, "tag": "Edit"
					}));
			};
			el.appendChild(child);
			break;
		default:
			el.appendChild(document.createTextNode("unsupported ui node: " + ui.tag));
			break;
	}
}

window.onload = function() {
	// Create websocket
	var host = window.location.hostname;
	var uri = 'ws://' + (host == '' ? 'localhost' : host) + ':8080/';
	const socket = "MozWebSocket" in window ? new MozWebSocket(uri) : new WebSocket(uri);

	//On open
	socket.addEventListener('open', function (event) {
		socket.send(JSON.stringify({"entrypoint": window.location.pathname, "tag": "Init"}));
	});
	
	//Received new UI
	socket.addEventListener('message', function (event) {
		var msg = JSON.parse(event.data);
		console.log("message: ", event.data);
		switch (msg.tag) {
			case "ErrorMsg":
				document.getElementById("viewport").innerHTML = '';
				document.getElementById("viewport").appendChild = document.createTextNode("server error: " + msg.errorMsg);
				break;
			case "Update":
				document.getElementById("viewport").innerHTML = '';
				renderUI(socket, msg.newUI, document.getElementById("viewport"));
				break;
			default:
				console.log('Unknown message from server ', msg);
		}
	});
};
